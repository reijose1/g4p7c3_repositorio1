
from .serializersInscripcion import InscripcionSerializer
from .serializersCategorias import CategoriaSerializer
from .serializersLugar import LugarSerializer
from .serializersPersona import PersonaSerializer
from .serializersTipoevento import TipoEventoSerializer
from .serializersEvento import EventoSerializer


