# Categoria
from .categoria.categoriaAPIView import CategoriaCreateAPIView, CategoriaAPIView, CategoriaDetailAPIView, CategoriaUpdateAPIView, CategoriaDeleteAPIView

# Evento
from .evento.eventoAPIView import EventoCreateAPIView, EventoAPIView, EventoDetailAPIView, EventoUpdateAPIView, EventoDeleteAPIView

# Inscripcion
from .inscripcion.inscripcionAPIView import InscripcionCreateAPIView, InscripcionAPIView, InscripcionDetailAPIView, InscripcionUpdateAPIView, InscripcionDeleteAPIView

# Lugar
from .lugar.lugarAPIView import LugarCreateAPIView, LugarAPIView, LugarDetailAPIView, LugarUpdateAPIView, LugarDeleteAPIView

# Persona
from .persona.personaAPIView import PersonaCreateAPIView, PersonaAPIView, PersonaDetailAPIView, PersonaUpdateAPIView, PersonaDeleteAPIView

# Tipo Evento
from .tipoEvento.tipoEventoAPIView import TipoEventoCreateAPIView, TipoEventoAPIView, TipoEventoDetailAPIView, TipoEventoUpdateAPIView, TipoEventoDeleteAPIView
