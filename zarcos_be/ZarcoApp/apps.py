from django.apps import AppConfig


class ZarcoappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ZarcoApp'
